package ru.kharlamova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserChangePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change password for current user";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull final String newPassword  = TerminalUtil.nextLine();
        @NotNull final Optional<User> user = serviceLocator.getUserService().setPassword(userId, newPassword);
        System.out.println("[OK]");
    }

}
