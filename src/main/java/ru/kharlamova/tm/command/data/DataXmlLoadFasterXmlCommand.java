package ru.kharlamova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.dto.Domain;
import ru.kharlamova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-xml-load";
    }

    @Override
    public @Nullable String description() {
        return "Load data from XML.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
