package ru.kharlamova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.dto.Domain;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";
    @NotNull
    protected static final String FILE_BINARY = "./data.bin";
    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";
    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";
    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";
    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";
    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";
    @NotNull
    protected static final String FACTORY = "javax.xml.bind.context.factory";
    @NotNull
    protected static final String JAXB_CONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";
    @NotNull
    protected static final String MEDIA_TYPE = "eclipselink.media-type";
    @NotNull
    protected static final String APP_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new ObjectNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new ObjectNotFoundException();
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
    }

}
