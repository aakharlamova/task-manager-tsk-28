package ru.kharlamova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    public static final String APPLICATION_VERSION = "application.version";

    @NotNull
    public static final String APPLICATION_VERSION_DEFAULT = "1.0.0";

    @NotNull
    public static final String DEVELOPER_EMAIL = "application.email";

    @NotNull
    public static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    public static final String DEVELOPER_NAME = "application.developer";

    @NotNull
    public static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY)) return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.valueOf(value);
        }
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.valueOf(value);
        }
        @NotNull final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.valueOf(value);
    }

    @Override
    public @NotNull String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION)) return System.getenv(APPLICATION_VERSION);
        if (System.getProperties().containsKey(APPLICATION_VERSION)) return System.getProperty(APPLICATION_VERSION);
        return properties.getProperty(APPLICATION_VERSION, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperEmail() {
        if (System.getenv().containsKey(DEVELOPER_EMAIL)) return System.getenv(DEVELOPER_EMAIL);
        if (System.getProperties().containsKey(DEVELOPER_EMAIL)) return System.getProperty(DEVELOPER_EMAIL);
        return properties.getProperty(DEVELOPER_EMAIL, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperName() {
        if (System.getenv().containsKey(DEVELOPER_NAME)) return System.getenv(DEVELOPER_NAME);
        if (System.getProperties().containsKey(DEVELOPER_NAME)) return System.getProperty(DEVELOPER_NAME);
        return properties.getProperty(DEVELOPER_NAME, DEVELOPER_NAME_DEFAULT);
    }

}

