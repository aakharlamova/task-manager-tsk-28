package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.IBusinessService;
import ru.kharlamova.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    @Nullable
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

}
