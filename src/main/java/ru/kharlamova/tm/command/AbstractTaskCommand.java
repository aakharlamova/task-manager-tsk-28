package ru.kharlamova.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
        System.out.println("Start Date: " + task.getDateStart());
        System.out.println("Created: " + task.getCreated());
    }

}
