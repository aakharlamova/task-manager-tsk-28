package ru.kharlamova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.dto.Domain;
import ru.kharlamova.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-json-jaxb-load";
    }

    @Override
    public @NotNull String description() {
        return "Load data from JSON.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        System.setProperty(FACTORY, JAXB_CONTEXT_FACTORY);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_TYPE, APP_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
