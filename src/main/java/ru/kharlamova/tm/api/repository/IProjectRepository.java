package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.api.IBusinessRepository;
import ru.kharlamova.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
